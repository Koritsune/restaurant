﻿using Restaurant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantExample
{
    class Program
    {
        static int messages = 2;

        static void Main(string[] args)
        {
            Server server = new Server();
            Waiter waiter = new Waiter();

            server.ServerMessageReceived += server_ServerMessageReceived;
            server.AcceptConnections("0.0.0.0", 6114);

            waiter.MessageReceived += waiter_MessageReceived;
            waiter.ConnectToServer("127.0.0.1", 6114);


            server.SendMessage("Hi from the Server!");
            waiter.SendMessage("Hi from the Waiter!");

            while (messages > 0) ;

            Console.WriteLine("Press any key to continue ...");
            Console.ReadLine();

            server.CloseConnections();
            waiter.Stop();
        }

        static void waiter_MessageReceived(object source, Waiter.MessageReceivedEvent e)
        {
            Console.WriteLine("Waiter received message: " + e.message);
            messages--;
        }

        static void server_ServerMessageReceived(object source, Server.ServerMessageReceivedEvent e)
        {
            Console.WriteLine("Server received message: " + e.message);
            messages--;
        }
    }
}
