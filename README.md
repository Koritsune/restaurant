#Restaurant
#Library Description

This is a C# library written to simplify networking for programs. This was designed for small games such as baord games but it could be used for anything. Attached is a small example on how you can quickly get a connection running with ease!

- - -

## Protocol for Issues.

Simply take an issue and assign yourself as an assignee so all other programmers know you are working on it. The same goes that if you see another programmers name on an issue you will not attempt to do their work for them without their explicit permission.

### Creating issues

Issues can be created by anyone, please try to keep them at as much of a base level as possible (within reason) do not assign these issues to another coder without explicit permission and be sure to keep the explanation for it as simple and clear as possible.